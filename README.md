# GitLab CI Demo

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

We just add `.gitlab-ci.yml` and some other configuration to demonstrate how
we can have CI/CD pipeline with Gitlab CI.

## Pre Requirement

You should set these variables on the Gitlab CI/CD settings.

NOTE: We use the Gitlab container as a registry. so the env would be set like this

- `CI_REGISTRY` the registry address `registry.gitlab.com`
- `CI_REGISTRY_USER` your gitlab username like `atahani`
- `CI_REGISTRY_PASSWORD` it can be gitlab password but we highly recommend to use [personal access token](https://gitlab.com/profile/personal_access_tokens) with api and read_registry scope.
- `APP_URL` we set this env for web app address in production environment like `https://gitlab-ci.funnydemo.ir/`
